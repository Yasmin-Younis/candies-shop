
public class Sundae extends IceCream{
	

	String Topping;
	int priceTopping;

	public  Sundae(String n , int price, String Toppingname, int p) {
		super(n , price);
		this.Topping = Toppingname ;
		this.priceTopping = p;	
	}
	public int getCost() {
		
		int cost =0;
		cost = this.priceTopping + super.getCost();
		return cost;
	}
	
}
