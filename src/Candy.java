
public class Candy  extends DessertItem{
	//String name;
	int pricePerPound;
	double weight;
	
public Candy( String n , double w , int p) {
	super (n);
	this.weight= w;
	this.pricePerPound = p;
	
}
public int getCost() {
	int  finalPrice =0;
	finalPrice =	(int)Math.round (this.weight*this.pricePerPound);
	return finalPrice;
}

}
