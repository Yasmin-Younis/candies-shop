import java.util.*;
public class Checkout {

	ArrayList <DessertItem> cash  = new ArrayList <DessertItem>();
	public Checkout() {

	}
	public int numberOfItems() {
		int n = cash.size();
		return n;
	}
	public void enterItem(DessertItem item){
		cash.add (item);
	}
	public void clear() {
		cash.clear();
	}
	public int totalCost() {
		int sum = 0;
		for (int i = 0; i< cash.size(); i++){
			sum += cash.get(i).getCost();
		}
		return  sum;
	}
	public int totalTax() {
		return (int)Math.round(totalCost() * DessertShoppe.TAX_RATE /100); 
	}
	public void print(){
		System.out.println("----------"+DessertShoppe.STORE_NAME+"----------");
		for (int i = 0; i< cash.size();i++){
			System.out.println( cash.get(i).getName()+"   "+ DessertShoppe.cents2dollarsAndCents(cash.get(i).getCost()));
		}
		System.out.println("Total Cost:"+ this.totalCost() +"   "+"Total Taxes:"+ this.totalTax());

			
	}
	
}
